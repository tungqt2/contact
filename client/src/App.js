import React, { useState } from 'react'
import './App.css';
import FormContact from './views/FormContact';
import ListContact from './views/ListContact';

function App() {
  const [data,setData] = useState([])
  const onCreateData = (e)=>{
    const newData = [...data]
    newData.push(e[0])
    setData(newData)
  }
  React.useEffect(()=>{
      async function fetchData(){
          try {
              const response = await fetch("http://127.0.0.1:3001/contact")
              const JsonData = await response.json()
              if(JsonData.status === 1){
                  setData(JsonData.data)
              }else{}
          } catch (error) {}
  }
  fetchData()
},[])
  return (
    <div className="container card-body">
      <div className="row mt-5">
        <div className="col">
          <FormContact onCreateData={onCreateData}></FormContact>
          
        </div>
        <div className="col">
          <ListContact data={data}></ListContact>
        </div>
      </div>
    </div>
  );
}

export default App;
