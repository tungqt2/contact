import React from 'react'
function convertTime(str){
    const date = new Date(str)
    // console.log()
    return `${(date).getDate()}-${(date).getMonth()+1}-${(date).getFullYear()} ${(date).getHours()}:${(date).getMinutes()}:${(date).getSeconds()}`
}
function ListContact({data}) {
  

    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Họ và tên</th>
                    <th scope="col">Email</th>
                    <th scope="col">Số ĐT</th>
                    <th scope="col">Ngày tạo</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((x,index)=>(
                    <tr>
                        <th scope="row">{index}</th>
                        <td>{x.name}</td>
                        <td>{x.email}</td>
                        <td>{x.phone}</td>
                        <td>{convertTime (x.create_at)}</td>
                        
                    </tr>
                    ))}

                </tbody>
            </table>

        </div>
    )
}

export default ListContact
