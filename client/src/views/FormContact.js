import React from 'react'

function FormContact({onCreateData}) {

    const [name,setName] = React.useState('')
    const [phone,setPhone] = React.useState('')
    const [email,setEmail] = React.useState('')
    const [content,setContent] = React.useState('')

    const defautValue = ()=>{
        setName('')
        setPhone('')
        setEmail('')
        setContent('')
    }
    const onClickGui = async (req,res)=>{
        try{
            if(name ===''){
                alert('Người dùng chưa nhập họ và tên!')
            }else if(phone === ''){
                alert('Người dùng chưa nhập số điện thoại!')
            }else if(email === ''){
                alert('Người dùng chưa nhập Email!')
            }else if(content === ''){
                alert('Người dùng chưa nhập content!')
            }else{
                const response = await fetch("http://127.0.0.1:3001/contact",{
                    method:"POST",
                    headers:{"Content-Type":"application/json"},
                    body:JSON.stringify({name,phone,email,content})
                })
                const JsonData = await response.json()
                if(JsonData.status ===1){
                    alert("Thành công!")
                    onCreateData(JsonData.data)
                    defautValue()
                }else{
                    alert("Thất bại!")
                }
            }

        }catch(error){

        }
    }
    return (
        <div className="card" style={{ width: '100%' }}>
            <div className="card-body">
                <div className="text-center">
                    <label style={{ fontWeight: 'bold', fontSize: '32px' }}>Liên hệ</label>
                </div>
                <div className="mb-3">

                    <input type="text" className="form-control"
                        placeholder="Họ và tên"
                        value={name}
                        onChange={e=>setName(e.target.value)}
                        id="exampleInputEmail1" aria-describedby="emailHelp" />
                </div>
                <div className="mb-3">

                    <input type="text" className="form-control"
                        placeholder="Email"
                        value={email}
                        onChange={e=>setEmail(e.target.value)}
                        id="exampleInputEmail1" aria-describedby="emailHelp" />
                </div>
                <div className="mb-3">

                    <input type="text" className="form-control"
                        placeholder="Điện thoại"
                        value={phone}
                        onChange={e=>setPhone(e.target.value)}
                        id="exampleInputEmail1" aria-describedby="emailHelp" />
                </div>
                <div className="mb-3">

                    <textarea type="text" className="form-control"
                        style={{height:'250px'}}
                        placeholder="Nội dung"
                        value={content}
                        onChange={e=>setContent(e.target.value)}
                        id="exampleInputEmail1" aria-describedby="emailHelp" />
                    </div>

                <div className="text-center">
                    <button type="submit" className="btn btn-primary"
                    onClick={()=>onClickGui()}
                    >Gửi</button>
                </div>
               

            </div>


        </div>
    )
}

export default FormContact
