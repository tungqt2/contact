CREATE TABLE public.contact
(
    id bigserial,
    name text,
    phone text,
    content text,
	email text,
	create_at timestamptz,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.contact
    OWNER to order0phi;