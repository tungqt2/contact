// Cách cò bay lả bay la, bay về cửa phủ bay ra cánh đồng =))

// Đọc dữ liệu từ file .env (dotenv) : file cấu hình
require('dotenv').config()

// Khởi tạo thư viện tạo các API để client truy xuất gọi về
const express = require('express');

// Tạo phương thức http chạy API trên Web      
const http = require('http');

// Gọi thư viện để thao tác với CSDL  (Postgress)
const Pool = require("pg").Pool;

// Cors là 1 pakage hết sức tiện lợi, cung cấp các middleware cho phép ta enable cors với nhiều option để tùy chỉnh và ngắn gọn cho express.  
// Cụ thể hơn thì cors chính là thiết lập các client khác đồng thời truy cập, cấp quyền xử dụng            
const cors = require('cors');               
                                            
var bodyParser = require('body-parser')
// bodyParser xây dựng yêu cấu(request) từ các đoạn dữ liệu mà nó nhận được bao gồm định dạng
// Từ Body
// application/x-www-form-urlencoded
// multipart/form-data
// application/json
// application/xml
// maybe some others

const port = process.env.PORT || 3001;
// Khởi tạo port cổng số để client truy cập

const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json());
// Tạo biến app phục vụ cho việc tạo ra các API của SERVER                      

// API có các phương thức GET, POST, PUT, DELETE

// Kết nối CSDL (POSTGRESS)

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    });
                   
// Thời gian hiện tại
const timeNow = `${(new Date()).getDate()}-${(new Date()).getMonth()+1}-${(new Date()).getFullYear()} ${(new Date()).getHours()}:${(new Date()).getMinutes()}:${(new Date()).getSeconds()}`

// Thời gian lưu vào DB
const timeNowDB = `${(new Date()).getFullYear()}-${(new Date()).getMonth()+1}-${(new Date()).getDate()} ${(new Date()).getHours()}:${(new Date()).getMinutes()}:${(new Date()).getSeconds()}`
    

// Phương thức POST
app.post('/contact' , async(req,res)=>{
    try {
        // Lấy giá trị {name, phone, content} truyền từ client xuống server
        const {name, phone, email, content} = req.body //(req.body) tương ứng là bắt yêu cầu(request) từ client
        console.log({name, phone, email, content})
        // Gọi biến pool đã kết nối csdl trước đó để thực hiện câu lệnh query
        const newQuery = await pool.query(`
            insert into contact(
                name,phone,content,email,create_at)
                values(N'${name}',N'${phone}',N'${content}',N'${email}','${timeNowDB}')
        `)

        const newQueryRes = await pool.query(`
                select * from contact where phone = N'${phone}'
                ORDER BY create_at DESC
                LIMIT 1
        `)
        // Truyền kết quả trả lại về bên client 
        res.json({
            status : newQuery.rowCount > 0 ? 1 : 0,
            data : newQuery.rowCount > 0 ?  newQueryRes.rows  : []    
        })
    } catch (error) {
        // Trả lỗi nếu do phát sinh bên ngoài
        res.json({
            status : 0,
        })
        console.log(error)
    }
})


// Phương thức GET
app.get('/contact' , async(req,res)=>{
    try {
        const newQuery = await pool.query(`
            select * from contact
        `)
        // Truyền kết quả trả lại về bên client 
        res.json({
            status : newQuery.rowCount > 0 ? 1 : 0,
            data :newQuery.rowCount > 0 ?  newQuery.rows : []  
        })
    } catch (error) {
        // Trả lỗi nếu do phát sinh bên ngoài
        res.json({
            status : 0,
        })
        console.log(error)
    }
})

app.listen(port, () => console.log(`Start server on port ${port}`));
