@ECHO OFF
(
    @ECHO INSTALL PACKAGE . . .
    npm install express http cors body-parser pg
    @ECHO START SERVER . . .
    npm start
)
pause